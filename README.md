
# Selamat Datang di Service produk pada Aplikasi Penjualan
Service ini bertujuan untuk mengelola data-data Produk yang ada dan menyediakan API sederhana sesuai dengan kebutuhan
Service ini berjalan dengan framework Flask dan dengan ORM SQLAlchemy
Package yang digunakan:

Flask
json

Fungsi yang ada pada service ini adalah:

View all produk -> token required
View user by produkId
View user by produkKategori
Insert new produk
Update produk
Delete produk
