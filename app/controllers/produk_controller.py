from logging import debug
from app.models.produk_model import Produk
from flask import Flask, jsonify, request
from flask_jwt_extended import *
import json, datetime


mysqldb = Produk()

# @jwt_required()
def showsProduk():
    dbresult = mysqldb.showProduk()
    result = []
    for item in dbresult:
        produk = {
            'produkId' : item.produkId,
            'namaProduk' : item.namaProduk,
            'kategoriProduk' : item.kategoriProduk,
            'jumlahProduk' : item.jumlahProduk,
            'hargaProduk' : item.hargaProduk,
            'kategoriId' : item.kategoriId,
           
        }
        result.append(produk)
    return jsonify(result)

def showProduk(**params):
    dbresult = mysqldb.showProdukById(**params)
    for item in dbresult:
        produk = {
            'produkId' : item.produkId,
            'namaProduk' : item.namaProduk,
            'kategoriProduk' : item.kategoriProduk,
            'jumlahProduk' : item.jumlahProduk,
            'hargaProduk' : item.hargaProduk,
            'kategoriId' : item.kategoriId,
           
        }
        return jsonify(produk)

def showProdukKategori(**params):
    lstResult = []
    dbresult = mysqldb.showProdukByKategori(**params)
    for item in dbresult:
        produk = {
            'produkId' : item.produkId,
            'namaProduk' : item.namaProduk,
            'kategoriProduk' : item.kategoriProduk,
            'jumlahProduk' : item.jumlahProduk,
            'hargaProduk' : item.hargaProduk,
            'kategoriId' : item.kategoriId,
           
        }
        lstResult.append(produk)
    return jsonify(lstResult)

def showProdukName(**params):
    lstResult = []
    dbresult = mysqldb.showProdukByName(**params)
    if dbresult is not None:
        for item in dbresult:
            produk = {
                'produkId' : item.produkId,
                'namaProduk' : item.namaProduk,
                'kategoriProduk' : item.kategoriProduk,
                'jumlahProduk' : item.jumlahProduk,
                'hargaProduk' : item.hargaProduk,
                'kategoriId' : item.kategoriId,
            
            }
            lstResult.append(produk)
    else: 
        return {
            "message" : "Produk tidak ditemukan"
        }
    return jsonify(lstResult)
# def showprodukEmail(**params):
#     dbresult = mysqldb.showprodukByEmail(**params)
#     for result in dbresult:
#         data = {
#             "customerId" : result.customerId,
#             "namaDepan" : result.namaDepan,
#             "namaBelakang" : result.namaBelakang,
#             "email" : result.email
#         }
#         return jsonify(data)

def insertNewProduk(**param):
    mysqldb.insertProduk(**param)
    return jsonify({"message" : "success"})

def updateProduk(**param):
    mysqldb.updateProdukById(**param)
    return jsonify({"message" : "Success"})

def deleteProduk(**param):
    mysqldb.deleteProdukById(**param)
    return jsonify({"message" : "Success"})

    