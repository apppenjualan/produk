from flask.json import jsonify
from app import app
from app.controllers import produk_controller
from flask import Blueprint, request

produk_blueprint = Blueprint("produk_controller", __name__)

@app.route('/produk', methods =["GET"])
def shows():
    return produk_controller.showsProduk()

@app.route('/produkbyid', methods = ["GET"])
def show():
    param = request.json
    return produk_controller.showProduk(**param)

@app.route('/produkbykategori', methods = ["GET"])
def showByKategori():
    param = request.json
    return produk_controller.showProdukKategori(**param)

@app.route('/produkbyname', methods = ["GET"])
def showByName():
    param = request.json
    return produk_controller.showProdukName(**param)

@app.route('/produk/insert', methods = ["POST"])
def insert():
    param = request.json
    return produk_controller.insertNewProduk(**param)
# @app.route('/userbyemail', methods = ["GET"])
# def showByEmail():
#     param = request.json
#     return customer_controller.showUserEmail(**param)

@app.route('/produk/update', methods = ["POST"])
def update():
    param = request.json
    return produk_controller.updateProduk(**param)

@app.route('/produk/delete', methods = ["POST"])
def delete():
    param = request.json
    return produk_controller.deleteProduk(**param)

# @app.route('/user/requesttoken', methods = ["GET"])
# def token():
#     param = request.json
#     return customer_controller.token(**param)

