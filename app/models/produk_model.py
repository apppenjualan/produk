from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from sqlalchemy.sql.functions import user
import re

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:Pythonbackend@localhost:3306/penjualan', echo = True)
Session = sessionmaker(bind = engine)
session = Session()

if session:
    print("Connection Success")

class Produk(Base):
    __tablename__ = 'produk'
    produkId = Column(Integer, primary_key= True)
    namaProduk = Column(String)
    kategoriProduk = Column(String)
    jumlahProduk = Column(String)
    hargaProduk = Column(Integer)
    kategoriId = Column(Integer)

    def showProduk(self):
        result = session.query(Produk).all()
        return result
    
    def showProdukById(self, **params):
        prodId = params['produkId']
        result_query = session.query(Produk).filter(Produk.produkId == int(prodId))
        return result_query
    
    def showProdukByKategori(self, **params):
        kategori = params['kategoriProduk']
        result_query = session.query(Produk).filter(Produk.kategoriProduk == kategori)
        return result_query

    def showProdukByName(self, **params):
        nama = params['namaProduk']
        result_query = session.query(Produk).filter(Produk.namaProduk == nama)
        return result_query

    def insertProduk(self, **params):    
        session.add(Produk(**params))
        session.commit()
    
    def updateProdukById(self, **params):
        produkId = params['produkId']
        result_query = session.query(Produk).filter(Produk.produkId == produkId).one()
        result_query.namaProduk = params['namaProduk']
        session.commit()

        result_query = session.query(Produk).filter(Produk.produkId == int(produkId)).one()
        return result_query

    def deleteProdukById(self, **params):
        id = params['produkId']
        result_query = session.query(Produk).filter(Produk.produkId == id).one()
        session.delete(result_query)
        session.commit()

        result_query = session.query(Produk).all()
        return result_query